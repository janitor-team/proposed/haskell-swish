#!/usr/bin/make -f

DEB_ENABLE_TESTS = yes

include /usr/share/cdbs/1/rules/debhelper.mk
-include /usr/share/cdbs/1/class/hlibrary.mk

# helper macros for GHC dependencies
# example use: foo bar,dev,prof doc,>=,1.2,<<,1.3 baz
expand-or-strip-ghc-dep = $(foreach expandsuffix,$1,\
 $(comma) libghc-$3-$(expandsuffix) $(if $4,($4 $5))\
 $(if $6,\
  $(comma) libghc-$3-$(expandsuffix) ($6 $7)))\
 $(foreach stripsuffix,$2,\
  $(comma) libghc-$3-$(stripsuffix))
expand-ghc-deps = $(foreach dep,$(sort $3),$(strip \
 $(call expand-or-strip-ghc-dep,\
  $1,\
  $2,$(strip \
  $(word 1,$(subst $(comma),$(space),$(dep)))),$(strip \
  $(word 2,$(subst $(comma),$(space),$(dep)))),$(strip \
  $(word 3,$(subst $(comma),$(space),$(dep)))),$(strip \
  $(word 4,$(subst $(comma),$(space),$(dep)))),$(strip \
  $(word 5,$(subst $(comma),$(space),$(dep))))) \
 $4))

# Needed by upstream build
ghc-deps += hashable,<<,1.6
ghc-deps += network-uri,<<,2.8
ghc-deps += polyparse,<<,1.14
ghc-deps += semigroups,<<,0.21
ghc-deps += intern,<<,1.0
bdeps = ghc, ghc-prof
bdeps +=, $(call expand-ghc-deps,dev,prof,$(ghc-deps))

# Needed by upstream testsuite
ghc-deps-test = hunit test-framework test-framework-hunit
bdeps-test = $(call expand-ghc-deps,dev,,$(ghc-deps-test),<!nocheck>)

# Needed by upstream documentation build
bdeps-indep = ghc-doc
# these are undeclared but warned about at build time
# * nats avoided (see bug#809032)
#ghc-deps-doc += nats
ghc-deps-doc += unordered-containers
bdeps-indep +=, $(call expand-ghc-deps,,doc,$(ghc-deps) $(ghc-deps-doc))

# Needed for our packaging
bdeps +=, haskell-devscripts, help2man

# TODO: actually use CDBS_BUILD_DEPENDS_INDEP, instead of hardcoding
CDBS_BUILD_DEPENDS +=, $(bdeps), $(bdeps-test), $(bdeps-prof)
CDBS_BUILD_DEPENDS_INDEP +=, $(bdeps-indep)

# generate manpage based on --help of script itself
# (using wrappers to emulate --version and provide --description)
post-build/swish:: swish := $(wildcard debian/tmp-inst-*/usr/bin/Swish)
post-build/swish:: debian/Swish.1
debian/Swish.1:
	help2man \
		--name="read, merge, write, compare and process RDF graphs" \
		--help-option=-h --version-option=-v \
		--no-info --output=$@ $(swish)
clean::
	rm -f debian/Swish.1

DEB_INSTALL_EXAMPLES_swish = scripts/*

# Strip hardcoded rpath
CDBS_BUILD_DEPENDS +=, chrpath
binary-strip/swish::
	find debian/$(cdbs_curpkg)/usr/bin -type f -execdir chrpath -d '{}' +
